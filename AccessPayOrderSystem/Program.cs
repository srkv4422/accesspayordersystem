﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using static AccessPayOrderSystem.Data;

namespace AccessPayOrderSystem
{
    class Program
    {
        static void Main(string[] args)
        {

            for (int i = 0; i <= 10; i++)
            {
                try
                {
                    foreach (Product item in Products())
                    {
                        Console.WriteLine(" Id : {0} ,  Name : {1}", item.Id, item.Name);
                    }
                    Console.WriteLine("Enter Product Id");
                    AccessPayOrderSystem.Process(Convert.ToInt32(Console.ReadLine()));
                    Console.WriteLine("\r\n");
                }
                catch (Exception)
                {
                    Console.WriteLine("Invalid Input, Please try again\r\n");
                }
            }
        }
    }

    public class AccessPayOrderSystem
    {

        public static void Process(int productId)
        {
            List<int> eventIds = new List<int>();

            var product = Products().Find(x => x.Id == productId);

            if (product.IsPhysicalProduct)
            {
                //Get all actions for that Physical Products
                eventIds.AddRange(PhysicalProductActions());
            }
            else
            {
                //Get all actions for that Non-Physical Products
                eventIds.AddRange(NonPhysicalProductActions());
            }
            //Get all actions for that productid
            var items = from item in ProductActions()
                        where item[0] == productId
                        select item[1];

            eventIds.AddRange(items);

            foreach (var id in eventIds)
            {
                Events type = (Events)id;
                // we can below events to event handler to process
                switch (type)
                {
                    case Events.GeneratedPackingSlip:
                        {
                            Console.WriteLine("Generated a packing slip");
                            break;
                        }
                    case Events.GenerateSlipForRoyaltyDepartment:
                        {
                            Console.WriteLine("Generated Slip For Royalty Department");
                            break;
                        }
                    case Events.ActivateMembership:
                        {
                            Console.WriteLine("Activated  Membership");
                            break;
                        }
                    case Events.UpdateMembership:
                        {
                            Console.WriteLine("Updated Membership");
                            break;
                        }
                    case Events.EmailOwner:
                        {
                            Console.WriteLine("Emailed Owner");
                            break;
                        }
                    case Events.FreeFirstAidVideoPackingSlip:
                        {
                            Console.WriteLine("'First Aid' video added to the packing slip");
                            break;
                        }
                    case Events.GenerateCommissionPaymentToTheAgent:
                        {
                            Console.WriteLine("Generated Commission Payment To The Agent");
                            break;
                        }
                    case Events.GeneratedPackingSlipForShipping:
                        {
                            Console.WriteLine("Generated Packing Slip For Shipping");
                            break;
                        }
                     default:
                        {
                            Console.WriteLine("This product type is not is handled, mail sent to support team");
                            break;
                        }
                }
            }
        }
    }
}