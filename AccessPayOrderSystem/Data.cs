﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AccessPayOrderSystem
{
    public static class Data
    {

        public static List<Product> Products()
        {
            var products = new List<Product>();
            products.Add(new Product() { Id = 1, Name = "Video", IsPhysicalProduct = false });
            products.Add(new Product() { Id = 2, Name = "Book", IsPhysicalProduct = true });
            products.Add(new Product() { Id = 3, Name = "New  membership", IsPhysicalProduct = false });
            products.Add(new Product() { Id = 4, Name = "Update  membership", IsPhysicalProduct = false });
            products.Add(new Product() { Id = 5, Name = "Learning to Ski Video", IsPhysicalProduct = false });

            return products;
        }

        public static List<int> PhysicalProductActions()
        {
            var actions = new List<int>
            {
                (int)Events.GeneratedPackingSlipForShipping,
                (int)Events.GenerateCommissionPaymentToTheAgent
            };
            return actions;
        }

        public static List<int> NonPhysicalProductActions()
        {
            var actions = new List<int>
            {
                (int)Events.GeneratedPackingSlip
            };
            return actions;
        }

        public static int[][] ProductActions()
        {
            var actions = new int[][]
            {
                  //Product Id, Product Type
                  new int[]  {2,(int)Events.GenerateSlipForRoyaltyDepartment},
                  new int[]  {3,(int)Events.ActivateMembership},
                  new int[] { 4,(int)Events.UpdateMembership},
                  new int[] { 3,(int)Events.EmailOwner},
                  new int[] { 4,(int)Events.EmailOwner},
                  new int[] { 5,(int)Events.FreeFirstAidVideoPackingSlip},
                      
             };
            return actions;
        }

        public enum Events
        {
            GeneratedPackingSlip,//0
            GenerateSlipForRoyaltyDepartment,//1
            ActivateMembership,//2
            UpdateMembership,//3
            EmailOwner,//4
            FreeFirstAidVideoPackingSlip,//5
            GenerateCommissionPaymentToTheAgent,//6
            GeneratedPackingSlipForShipping,//7
        }
    }
}
