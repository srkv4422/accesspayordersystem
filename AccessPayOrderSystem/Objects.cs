﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessPayOrderSystem
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsPhysicalProduct { get; set; }

    }

    public class Action
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
